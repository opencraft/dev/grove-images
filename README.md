# grove-images

This repo tries to make it easier to create and publish Tutor images for custom fork/branches on Open edX.

## tutor-dind
A docker in docker image with tutor pre-installed. This will be used to build tutor images. This image has three tag formats.

- `CI_COMMIT_SHA`
- `tutor-<TUTOR_VERSION>`
- latest

It builds and pushes images with `CI_COMMIT_SHA` by default. But it only tags it with `TUTOR_VERSION` or `latest` after merged with `main` branch.

## tutor images

Tutor image build pipeline is generated based on `tutor-images/config.yml`. The structure of this file is following -

```yml
tutor-image-name:
    - repository: custom fork repository url
      tag_prefix: prefix for all image tags under this repository
      branches:
        - list of branches that will be used to build images
```

`tutor-image-name` — Tutor has a couple of images, ex — `openedx`, `openedx-forum` etc. For each tutor image we will have an array of repositories that we want to build image for. And those repositories can have an array of branches we want to build. Tag for image will be generated from branch name. Docker doesn't support `/` character in a tag. So it will be replaced with `-`. For example —
branch name `opencraft-release/koa.1` will become `opencraft-release-koa.1` tag.

If we set `tag_prefix` to `client-1` then the tag will become — `client-1-opencraft-release-koa.1`. This will be useful to manage custom forks/branches for clients.
