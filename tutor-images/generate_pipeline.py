import os
import yaml

config_file_path = os.path.join(os.path.dirname(__file__), 'config.yml')
generated_file_path = os.path.join(os.path.dirname(__file__), 'generated.yml')

with open(config_file_path) as file:
    config = yaml.safe_load(file)


# shared job description
common_job = '.tutor_common'
generated = {
    common_job: {
        'image': '$CI_REGISTRY_IMAGE/$TUTOR_DIND_IMAGE_NAME:tutor-$TUTOR_VERSION',
        'services': ['docker:dind'],
        'stage': 'build',
        'when': 'manual',
        'script': [
            'docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY',
            'docker pull $TUTOR_DOCKER_IMAGE_OPENEDX || true',  ## try to pull any previous image to use as cache.
            'tutor config save',
            'tutor images build $IMAGE_NAME',
            'tutor images push $IMAGE_NAME'
        ]
    }
}

for image_name, repos in config.items():
    for repo in repos:
        repo_url = repo.get('repository')
        tag_prefix = repo.get('tag_prefix')
        for branch in repo.get('branches', []):
            job_name = f'{image_name} - {repo_url}#{branch}'
            image_tag = branch.lower().replace('/', '-')
            # to support multiple fork.
            # if there is no tag prefix then it is our common branch
            if tag_prefix:
                image_tag = f'{tag_prefix}-{image_tag}'
            job = {
                'extends': common_job,
                'variables': {
                    'IMAGE_NAME': image_name,
                    'TUTOR_EDX_PLATFORM_REPOSITORY': repo_url,
                    'TUTOR_EDX_PLATFORM_VERSION': branch,
                    'TUTOR_DOCKER_REGISTRY': '$CI_REGISTRY',
                    'TUTOR_DOCKER_IMAGE_OPENEDX': f'$CI_REGISTRY_IMAGE/{image_name}:{image_tag}'
                }
            }
            generated[job_name] = job

# write generated config to yml file
with open(generated_file_path, 'w') as file:
    yaml.dump(generated, file)
